Rails.application.routes.draw do

  resources :profiles
  devise_for :users
  get '/users/activities', to: 'users#user_activities'
  resources :users
  resources :courses
  resources :institutions
  resources :teachers
  resources :attachments
  resources :activities
  resources :subjects

  root 'activities#index'

end
