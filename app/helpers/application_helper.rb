module ApplicationHelper
	def user_profile_of(user)
		Profile.find_by_user_id(user)
	end
end
