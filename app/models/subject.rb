class Subject < ActiveRecord::Base
	has_many :activities
	belongs_to :teacher
	belongs_to :course
end
