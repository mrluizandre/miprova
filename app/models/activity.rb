class Activity < ActiveRecord::Base
	has_many :attachments, dependent: :destroy
	belongs_to :subject
	belongs_to :user

	enum activity_type: [:exam, :test, :list, :code]

	accepts_nested_attributes_for :attachments

	def self.activity_type_for_select
		activity_types.map do |activity_type, _|
		  [I18n.t("activerecord.attributes.#{model_name.i18n_key}.activity_types.#{activity_type}"), activity_type]
		end
	end

end
