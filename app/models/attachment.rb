class Attachment < ActiveRecord::Base
	belongs_to :activity
	
	mount_uploader :file, AttachmentFileUploader

end
