class User < ActiveRecord::Base
  before_create :set_user_role
  before_create :build_default_profile
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :activities
  has_one :profile

  ROLES = %w[admin moderator publisher banned].freeze

  private
    def set_user_role
      self.role = 'publisher'
    end

    def build_default_profile
      build_profile
      true
    end

end
