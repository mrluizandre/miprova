class Course < ActiveRecord::Base
	has_many :subjects
	belongs_to :institution
end
