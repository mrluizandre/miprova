json.extract! attachment, :id, :activity_id, :created_at, :updated_at
json.url attachment_url(attachment, format: :json)