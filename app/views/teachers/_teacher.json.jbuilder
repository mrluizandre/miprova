json.extract! teacher, :id, :name, :lattes, :created_at, :updated_at
json.url teacher_url(teacher, format: :json)