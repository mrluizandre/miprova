json.extract! profile, :id, :user_id, :name, :intitution_id, :bio, :logo, :birthday, :facebook, :twitter, :linkedin, :created_at, :updated_at
json.url profile_url(profile, format: :json)