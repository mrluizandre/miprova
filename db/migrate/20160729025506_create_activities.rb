class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :name
      t.text :description
      t.integer :type
      t.integer :year
      t.integer :period
      t.integer :subject_id

      t.timestamps null: false
    end
  end
end
