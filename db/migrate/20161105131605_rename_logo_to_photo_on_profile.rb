class RenameLogoToPhotoOnProfile < ActiveRecord::Migration
  def change
  	rename_column :profiles, :logo, :photo
  end
end
