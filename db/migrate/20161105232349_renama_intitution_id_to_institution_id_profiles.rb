class RenamaIntitutionIdToInstitutionIdProfiles < ActiveRecord::Migration
  def change
  	  	rename_column :profiles, :intitution_id, :institution_id
  end
end
