class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string :name
      t.integer :intitution_id
      t.text :bio
      t.string :logo
      t.date :birthday
      t.string :facebook
      t.string :twitter
      t.string :linkedin

      t.timestamps null: false
    end
  end
end
